<!DOCTYPE html>
<html lang="ru">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SP25</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- My CSS -->
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <!-- DESCRIPTION -->
    <div class="col-lg-4 mx-lg-auto mt-2">
   
        <div class="form">
            <fieldset class="scheduler-border-order">
                <legend class="scheduler-border">HTML-Форма </legend>
                <form action="index.php" method="POST">
                <?php
                    if (!empty($messages)) {
                        print('<div id="messages">');
                        // Выводим все сообщения.
                        foreach ($messages as $message) {
                        print($message);
                        }
                    print('</div>');
                    }
                // Далее выводим форму отмечая элементы с ошибками классом error
                // и задавая начальные значения элементов ранее сохраненными.
                ?>
                    <!-- Name -->
                    <div class="col">
                        <div class="form-group">
                            <label for="name">Имя:</label>
                            <input type="text" class="form-control" name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" />
                        </div>
                    </div>
                    <!-- Email -->
                    <div class="col">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" />
                        </div>
                    </div>
                    <!-- Birthday -->
                    <div class="col">
                        <div class="form-group">
                            <label for="birthday">Дата рождения:</label>
                            <input <?php if ($errors['birthday']) {print 'class="error"';} ?>  type="date" name = "birthday" value = "<?php print $values['birthday']; ?>">
                        </div>
                    </div>
                    <!-- Sex -->
                    <div class="col">
                        <div id="radio-div1" class="form-group">
                            <label for="sex"<?php if ($errors['sex']) {print 'class="error"';} ?> >Пол:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sex" id="sex1"
                                    value="M" <?php if ($values['sex'] == 'M') print("checked"); ?> >
                                <label class="form-check-label" for="sex1">Мужской</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="sex" id="sex2"
                                    value="W" <?php if ($values['sex'] == 'W') print("checked"); ?> >
                                <label class="form-check-label" for="sex2">Женский</label>
                            </div>
                        </div>
                    </div>
                    <!-- Limb -->
                    <div class="col">
                        <div id="radio-div2" class="form-group">
                            <label for="limb" <?php if($errors['limb']) print 'class="error"'; ?> >Количество конечностей:</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb1"
                                    value="1" <?php if ($values['limb'] == 1) print("checked"); ?> >
                                <label class="form-check-label" for="limb1">1</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb2"
                                    value="2" <?php if ($values['limb'] == 2) print("checked"); ?> >
                                <label class="form-check-label" for="limb2">2</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb3"
                                    value="3" <?php if ($values['limb'] == 3) print("checked"); ?> >
                                <label class="form-check-label" for="limb3">3</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="limb" id="limb4"
                                    value="4" <?php if ($values['limb'] == 4) print("checked"); ?> >
                                <label class="form-check-label" for="limb4">4</label>
                            </div>
                        </div>
                    </div>
                    <!-- SuperPowers -->
                    <div class="form-group">
                        <label for="superpowers">Сверхспособности(множественный выбор из списка):</label>
                        <select multiple class="form-control" name="superpowers[]" <?php if ($errors['superpowers']) {print 'class="error"';} ?> >
                        <option value="Immortal"
                            <?php   
                                if(!empty($values['superpowers'])){
                                if(in_array('Immortal', json_decode($values['superpowers'])))
                                print 'selected';          
                                }
                            ?> 
                            >Бессмертие 
                        </option>
                        <option  value="Walk through walls"
                            <?php   
                                if(!empty($values['superpowers'])){
                                if(in_array('Walk', json_decode($values['superpowers'])))
                                print 'selected';          
                                }
                            ?>  
                            >Прохождение сквозь стены
                        </option> 

                        <option value="Levitation" 
                            <?php   
                                if(!empty($values['superpowers'])){
                                if(in_array('Levitation', json_decode($values['superpowers'])))
                                print 'selected';          
                                }
                            ?>  
                            >Левитация
                        </option> 
                        <option value="Telekinez" 
                            <?php   
                                if(!empty($values['superpowers'])){
                                if(in_array('Telekinez', json_decode($values['superpowers'])))
                                print 'selected';          
                                }
                            ?>  
                            >Телезинез
                        </option> 
                        </select>
                    </div>
                    <!-- Bio -->
                    <div class="form-group">
                        <label for="bio">Биография:</label>
                        <textarea class="form-control" name="bio" rows="5" <?php if ($errors['bio']) {print 'class="error"';} ?>><?php print $values['bio']; ?> </textarea>
                    </div>
                    <!-- Check -->
                    <div class="form-check">
                        <input type="checkbox" name="checkbox" class="form-check-input" id="Check1" <?php if(!empty($values['checkbox'])) print 'checked';?> >
                        <label class="form-check-label" for="Check1" <?php if ($errors['checkbox']) {print 'class="error"';} ?> >С контрактом ознакомлен(а)</label>
                    </div>
                    <!-- Button -->
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </fieldset>
        </div>
    </div>
    </div>
    </body>
</html>