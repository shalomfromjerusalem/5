<?php
header('Content-Type: text/html; charset=UTF-8');
$user = 'u17333';
$pass = '7038049';
$db = new PDO('mysql:host=localhost;dbname=u17333', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
function is_superpowers_right(array $superpowers){
  $right_superpowers = ['Immortal', 'Walk', 'Levitation', 'Telekinez'];
  if(count($superpowers) == 0) {
  return false;
  } else {
  foreach($superpowers as $ability_key => $ability_value){
    if(!in_array($ability_value, $right_superpowers))
     return false;
  }
  return true;
}
}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 1);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    $messages[] = '<div class="alert-box success" style="text-align:center">Спасибо, результаты сохранены.</div>';
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf('<div class="alert-box success" style="text-align:center; padding: 10px;">Вы можете <a href="login.php">войти</a> для изменения данных.<br> Ваш логин <strong>%s</strong>
        и пароль <strong>%s</strong>.</div>',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['birthday'] = !empty($_COOKIE['birthday_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limb'] = !empty($_COOKIE['limb_error']);
  $errors['superpowers'] = !empty($_COOKIE['superpowers_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
  if ($errors['name']) {
    setcookie('name_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректное имя. Имя должно включать в себя только символы кириллицы      и английские буквы </div>';
  }
  if ($errors['email']) { 
    setcookie('email_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректный email. Email должен быть в формате: 
    example@domen.ru, допускаются цифры и \'_\' на месте "example"
    </div>';
  }
  if ($errors['birthday']) {
    setcookie('birthday_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректная дата. Дата должна быть в формате гггг-мм-дд</div>';
  }
  if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="errormsg">Выберите пол.</div>';
  }
  if ($errors['limb']) {
    setcookie('limb_error', '', 100000);
    $messages[] = '<div class="errormsg">Выберите количество конечностей.</div>';
  }
   if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages[] = '<div class="errormsg">Некорректная биография. Биография должна содержать от 5 символов
    (А-Я, а-я, A-Z, a-z,0-9, " ", _)
    </div>';
  }
  if ($errors['superpowers']) {
    setcookie('superpowers_error', '', 100000);
    $messages[] = '<div class="errormsg">Выберите сверхспособности.</div>';
  }
   if ($errors['checkbox']) {
    setcookie('checkbox_error', '', 100000);
    $messages[] = '<div class="errormsg">Вы не поставили галочку.</div>';
  }
  $values = array();

  $values['name'] = empty($_COOKIE['name_value']) || 
  !preg_match('/^[A-Za-zА-Яа-я]+$/u',$_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];

  $values['email'] = empty($_COOKIE['email_value']) || 
  !preg_match('/^[A-Za-z_0-9]+@[A-Za-z]+\.[A-Za-z]+$/u',$_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  
  $values['birthday'] = empty($_COOKIE['birthday_value']) || 
  !preg_match('/^\d\d\d\d\-\d\d\-\d\d$/u',$_COOKIE['birthday_value']) ? '' : $_COOKIE['birthday_value'];

  $values['sex'] = empty($_COOKIE['sex_value']) || 
  !preg_match('/^[MW]$/u',$_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];

  $values['limb'] = empty($_COOKIE['limb_value']) || 
  !preg_match('/^[1234]$/u',$_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];

  $values['superpowers'] = empty($_COOKIE['superpowers_value']) || 
  !is_superpowers_right(json_decode($_COOKIE['superpowers_value']))  ? '' : ($_COOKIE['superpowers_value']);

  $values['bio'] = empty($_COOKIE['bio_value']) || 
  !preg_match('/^[A-Za-z_0-9А-Яа-я\n\r\s]{5,}$/u',$_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];

  $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : $_COOKIE['checkbox_value'];

  if (!in_array(1, $errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
        $md5pass = md5($_SESSION['pass']);
        $stmt = $db->prepare('SELECT name, email, birthday, sex, limb, superpowers, bio, checkbox
                            FROM user WHERE login = ? AND password = ?');
        $stmt->bindValue(1, $_SESSION['login'], PDO::PARAM_STR);
        $stmt->bindValue(2, $md5pass, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
        $values['name'] = $row['name'];
        $values['email'] = $row['email'];
        $values['birthday'] = $row['birthday'];
        $values['sex'] = $row['sex'];
        $values['limb'] = $row['limb'];
        $values['superpowers'] = $row['superpowers'];
        $values['bio'] = $row['bio'];
        $values['checkbox'] = $row['checkbox'];
    printf('<div class="alert-boxx notice" > Вход с логином %s, uid %d </div>', $_SESSION['login'], $_SESSION['uid']);
    echo '<div class="alert-boxx notice" > Вы можете <a href = "login.php">  выйти </a></div>';
  } 
 include('form.php');

}

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
 $errors = FALSE;

  if (!preg_match('/^[A-Za-zА-Яа-я]+$/u', $_POST['name'])) {
    setcookie('name_error', '1', 0);
    $errors = TRUE;
  }  else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60 * 12);
  }


 if (!preg_match('/^[A-Za-z_0-9]+@[A-Za-z]+\.[A-Za-z]+$/', $_POST['email'])) {
  setcookie('email_error', '1', 0);
  $errors = TRUE;
 } else {
  setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
 }

if (!preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $_POST['birthday'])) {
  setcookie('birthday_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('birthday_value', $_POST['birthday'], time() + 12 * 30 * 24 * 60 * 60);
}

if(empty($_POST['sex']))
$_POST['sex']='';
if (!preg_match('/^[MW]$/', $_POST['sex'])) {
  setcookie('sex_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
}                    
if (!preg_match('/^[A-Za-z_0-9А-Яа-я\n\r\s]{5,}$/u', $_POST['bio'])) {
  setcookie('bio_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
}
if(empty($_POST['limb']))
$_POST['limb']='';
if (!preg_match('/^[1234]$/', $_POST['limb'])) {
  setcookie('limb_error', '1', 0);
  $errors = TRUE;
} else {
  setcookie('limb_value', $_POST['limb'], time() + 12 * 30 * 24 * 60 * 60);
}

if(empty($_POST['superpowers'])){
  setcookie('superpowers_error', '1', 0);
  $errors=TRUE;
} else if (!is_superpowers_right($_POST['superpowers'])){
  setcookie('superpowers_error', '1', 0);
  $errors=TRUE;
} else {
  setcookie('superpowers_value', json_encode($_POST['superpowers']), time() + 60 * 60 * 24 * 30 * 12);
}

if(empty($_POST['checkbox'])){
  setcookie('checkbox_error', '1', 0);
  $errors=TRUE;
} else {
  setcookie('checkbox_value', 'checked', time() + 60 * 60 * 24 * 30 * 12);
}

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
   header('Location: index.php');
    exit(); 
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('birthday_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limb_error', '', 100000);
    setcookie('superpowers_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('checkbox_error', '', 100000);
    
  }

  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');
  $user_abilities = json_encode($_POST['superpowers']);
if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
        try {
          $stmt = $db->prepare("UPDATE user SET name = ?,  email = ?,  birthday = ?,  sex = ?,  
          limb= ?,  superpowers = ?, bio = ?, checkbox = ? WHERE id = ?;");
          $stmt -> execute([ 
           $_POST['name'], 
           $_POST['email'], 
           $_POST['birthday'], 
           $_POST['sex'], 
           $_POST['limb'], 
           $user_abilities,
           $_POST['bio'],
           'ok',
           $_SESSION['uid']
           ]);
        }
        catch(PDOException $e){
          print('Error : ' . $e->getMessage());
          exit();
        }
  }else{
 $login = substr(uniqid(), 0 , 8);
 $pass = substr(md5(uniqid()), 0 , 8);
 setcookie('login', $login);
setcookie('pass', $pass); 
try {
  $stmt = $db->prepare("INSERT INTO user (login, password,name, email, birthday, sex, limb, superpowers, bio, checkbox) VALUES(:login, :pass, :name, :email, :birthday, :sex, :limb, :superpowers, :bio, :checkbox);");
  $result=$stmt -> execute(array(
  'login' => $login,
  'pass' => md5($pass),   
  'name'=>$_POST['name'], 
  'email' => $_POST['email'], 
  'birthday' => $_POST['birthday'], 
  'sex' => $_POST['sex'], 
  'limb' => $_POST['limb'], 
  'superpowers' => $user_abilities,
  'bio' => $_POST['bio'],
  'checkbox' => 'ok'));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
  }

  header('Location: index.php');
}

?>